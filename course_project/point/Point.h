//
// Created by Marseille Gulaya on 22/11/2018.
//

#ifndef COURSEPROJECT_VERTEX_H
#define COURSEPROJECT_VERTEX_H

class Point {
public:
    double x, y, z;

    Point() = default;
    Point(double x, double y, double z);

    ~Point() = default;

    double getX();
    double getY();
    double getZ();

    void setX(double x);
    void setY(double y);
    void setZ(double z);

    bool operator==(const Point& M) {
        return x == M.x && y == M.y && z == M.z;
    }
};

#endif //COURSEPROJECT_VERTEX_H
