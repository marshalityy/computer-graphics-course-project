//
// Created by Marseille Gulaya on 29/11/2018.
//

#ifndef COURSEPROJECT_TRANSFORMMATRIX_H
#define COURSEPROJECT_TRANSFORMMATRIX_H

#include "templates/Matrix.h"
#include "templates/Vector.h"
#include <cmath>

class TransformMatrix {
private:
    Matrix<double> matrix;

    Matrix<double> getTranslation(double dx, double dy, double dz);
    Matrix<double> getXRotation(double angle);
    Matrix<double> getYRotation(double angle);
    Matrix<double> getScaling(double ds);

public:
    TransformMatrix();
    explicit TransformMatrix(Matrix<double> matrix);
    ~TransformMatrix() = default;

    Matrix<double> getCurrentState();
    void set(Matrix<double> other);

    Matrix<double> getZRotation(double angle);
    void updateByRotation(double xa, double ya, double za);
    void updateByTranslation(double dx, double dy, double dz);
    void updateByScaling(double ds);
};

#endif //COURSEPROJECT_TRANSFORMMATRIX_H
