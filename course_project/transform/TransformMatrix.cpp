//
// Created by Marseille Gulaya on 29/11/2018.
//

#include <iostream>
#include "TransformMatrix.h"

TransformMatrix::TransformMatrix() {
    this->matrix = {{1, 0, 0, 0},
                    {0, 1, 0, 0},
                    {0, 0, 1, 0},
                    {0, 0, 0, 1}};
}

Matrix<double> TransformMatrix::getTranslation(double dx, double dy, double dz) {
    Matrix<double> translationMatrix = {{1,  0,  0,  0},
                                        {0,  1,  0,  0},
                                        {0,  0,  1,  0},
                                        {dx, dy, dz, 1}};

    return translationMatrix;
}

Matrix<double> TransformMatrix::getXRotation(double angle) {
    double c = cos(angle);
    double s = sin(angle);

    Matrix<double> rotationMatrix = {{1, 0,  0, 0},
                                     {0, c,  s, 0},
                                     {0, -s, c, 0},
                                     {0, 0,  0, 1}};

    return rotationMatrix;
}

Matrix<double> TransformMatrix::getYRotation(double angle) {
    double c = cos(angle);
    double s = sin(angle);

    Matrix<double> rotationMatrix = {{c, 0, -s, 0},
                                     {0, 1, 0,  0},
                                     {s, 0, c,  0},
                                     {0, 0, 0,  1}};

    return rotationMatrix;
}

Matrix<double> TransformMatrix::getZRotation(double angle) {
    double c = cos(angle);
    double s = sin(angle);

    Matrix<double> rotationMatrix = {{c,  s, 0, 0},
                                     {-s, c, 0, 0},
                                     {0,  0, 1, 0},
                                     {0,  0, 0, 1}};

    return rotationMatrix;
}

Matrix<double> TransformMatrix::getScaling(double ds) {
    Matrix<double> scalingMatrix = {{ds, 0,  0,  0},
                                    {0,  ds, 0,  0},
                                    {0,  0,  ds, 0},
                                    {0,  0,  0,  1}};

    return scalingMatrix;
}

void TransformMatrix::updateByScaling(double ds) {
    this->matrix *= getScaling(ds);
}

void TransformMatrix::updateByTranslation(double dx, double dy, double dz) {
    this->matrix *= getTranslation(dx, dy, dz);
}

void TransformMatrix::updateByRotation(double xa, double ya, double za) {
    Vector<double> temp;

    for (int i = 0; i < matrix.getRows() - 1; i++) {
        temp.pushBack(matrix(3, i));
    }

    for (int i = 0; i < matrix.getRows() - 1; i++) {
        matrix(3, i) -= temp[i];
    }

    matrix *= getXRotation(xa) * getYRotation(ya) * getZRotation(za);

    for (int i = 0; i < matrix.getRows() - 1; i++) {
        matrix(3, i) += temp[i];
    }
}

Matrix<double> TransformMatrix::getCurrentState() {
    return this->matrix;
}

void TransformMatrix::set(Matrix<double> other) {
    this->matrix = other;
}

TransformMatrix::TransformMatrix(Matrix<double> matrix) {
    this->matrix = matrix;
}
