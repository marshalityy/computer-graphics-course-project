//
// Created by Marseille Gulaya on 26/11/2018.
//

#ifndef COURSEPROJECT_MATRIX_H
#define COURSEPROJECT_MATRIX_H

#include <initializer_list>
#include <cmath>
#include <iostream>

template <typename T>
class Vector;

template<typename T>
class Matrix {
private:
    int rows, columns;
    T** data;

    // Выделение памяти
    T** allocator();

public:
    // Конструкторы
    Matrix();

    Matrix(int rows, int columns); // Стандартный конструктор
    explicit Matrix(int value); // Конструктор квадратной матрицы
    Matrix(const std::initializer_list<std::initializer_list<T>>& list);

    Matrix(const Matrix<T>& other); // Конструктор копирования

    // Деструктор
    ~Matrix();

    // ()
    const T& operator()(int i, int j) const;

    T& operator()(int i, int j);

    // == и !=
    bool operator==(const Matrix& other);

    bool operator!=(const Matrix& other);

    // =
    Matrix<T>& operator=(const Matrix& other);

    // + и -
    const Matrix<T> operator+(const Matrix& other) const;

    Matrix<T>& operator+=(const Matrix& other);

    const Matrix<T> operator-(const Matrix& other) const;

    Matrix<T>& operator-=(const Matrix& other);

    // *
    const Matrix<T> operator*(const Matrix& other) const;

    const Matrix<T> operator*(T value) const;

    Matrix<T>& operator*=(const Matrix& other);

    Matrix<T>& operator*=(T value);

    const Vector<T> operator*(const Vector<T>& other) const;

    int getRows();
    int getColumns();

    int getRows() const;
    int getColumns() const;

    Matrix<double> getInversed();

    void print();
};

template<typename T>
Matrix<T>::Matrix() {
    rows = 0;
    columns = 0;
    data = nullptr;
}

template<typename T>
Matrix<T>::Matrix(int rows, int columns) {
    this->rows = rows;
    this->columns = columns;

    this->data = this->allocator();
}

template<typename T>
Matrix<T>::Matrix(int value) {
    this->rows = value;
    this->columns = value;

    this->data = this->allocator();
}

template<typename T>
Matrix<T>::~Matrix() {
    for (int i = 0; i < rows; i++) {
        delete[] data[i];
    }

    delete[] data;
}

template<typename T>
Matrix<T>::Matrix(const Matrix& other) {
    this->rows = other.rows;
    this->columns = other.columns;
    this->data = this->allocator();

    for (int i = 0; i < other.rows; i++) {
        for (int j = 0; j < other.columns; j++) {
            this->data[i][j] = other.data[i][j];
        }
    }
}

template<typename T>
bool Matrix<T>::operator==(const Matrix& other) {
    bool flag = true;

    if (this->rows == other.rows && this->columns == other.columns) {
        for (int i = 0; i < this->rows && flag; i++) {
            for (int j = 0; j < this->columns; j++) {
                if (this->data[i][j] != other.data[i][j]) flag = false;
            }
        }
    } else flag = false;

    return flag;
}

template<typename T>
bool Matrix<T>::operator!=(const Matrix& other) {
    bool flag = false;

    if (this->rows == other.rows && this->columns == other.columns) {
        for (int i = 0; i < this->rows && !flag; i++) {
            for (int j = 0; j < this->columns; j++) {
                if (this->data[i][j] != other.data[i][j]) flag = true;
            }
        }
    } else flag = true;

    return flag;
}

template<typename T>
Matrix<T>& Matrix<T>::operator=(const Matrix& other) {
    if (this->data != nullptr) {
        for (int i = 0; i < rows; i++) {
            delete[] data[i];
        }

        delete[] data;
    }

    this->rows = other.rows;
    this->columns = other.columns;

    this->data = this->allocator();

    for (int i = 0; i < other.rows; i++) {
        for (int j = 0; j < other.columns; j++) {
            this->data[i][j] = other.data[i][j];
        }
    }

    return *this;
}

template<typename T>
const Matrix<T> Matrix<T>::operator+(const Matrix& other) const {
    if (rows != other.rows || columns != other.columns) {
        throw "Sizes are not equal.";
    }

    Matrix<T> temp(rows, columns);

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            temp.data[i][j] = data[i][j] + other.data[i][j];
        }
    }

    return temp;
}

template<typename T>
Matrix<T>& Matrix<T>::operator+=(const Matrix& other) {
    if (rows != other.rows || columns != other.columns) {
        throw "Sizes are not equal.";
    }

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            this->data[i][j] += other.data[i][j];
        }
    }

    return *this;
}

template<typename T>
const Matrix<T> Matrix<T>::operator-(const Matrix& other) const {
    if (rows != other.rows || columns != other.columns) {
        throw "Sizes are not equal.";
    }

    Matrix<T> temp(rows, columns);

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            temp.data[i][j] = data[i][j] - other.data[i][j];
        }
    }

    return temp;
}

template<typename T>
Matrix<T>& Matrix<T>::operator-=(const Matrix& other) {
    if (rows != other.rows || columns != other.columns) {
        throw "Sizes are not equal.";
    }

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            this->data[i][j] -= other.data[i][j];
        }
    }

    return *this;
}

template<typename T>
const Matrix<T> Matrix<T>::operator*(const Matrix& other) const {
    Matrix<T> temp(rows, columns);

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            temp.data[i][j] = 0;
            for (int k = 0; k < rows; k++) {
                temp.data[i][j] += this->data[i][k] * other.data[k][j];
            }
        }
    }

    return temp;
}

template<typename T>
const Matrix<T> Matrix<T>::operator*(const T value) const {
    Matrix<T> temp(rows, columns);

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            temp.data[i][j] = data[i][j] * value;
        }
    }

    return temp;
}

template<typename T>
Matrix<T>& Matrix<T>::operator*=(const Matrix& other) {
    Matrix<T> temp = *this;

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            this->data[i][j] = 0;
            for (int k = 0; k < rows; k++) {
                this->data[i][j] += temp.data[i][k] * other.data[k][j];
            }
        }
    }

    return *this;
}

template<typename T>
Matrix<T>& Matrix<T>::operator*=(const T value) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            this->data[i][j] *= value;
        }
    }

    return *this;
}

template<typename T>
T** Matrix<T>::allocator() {
    data = new T* [rows];

    for (int i = 0; i < rows; i++) {
        data[i] = new T[columns];
    }

    return this->data;
}

template<typename T>
const T& Matrix<T>::operator()(int i, int j) const {
    if (i >= rows || j >= columns || i < 0 || j < 0) {
        throw "Out of range.";
    }

    return data[i][j];
}

template<typename T>
T& Matrix<T>::operator()(int i, int j) {
    return data[i][j];
}

template<typename T>
Matrix<T>::Matrix(const std::initializer_list<std::initializer_list<T>>& list) {
    this->rows = static_cast<int>(list.size());
    this->columns = static_cast<int>(list.begin()->size());

    this->data = this->allocator();

    int i = 0, j = 0;
    for (const auto& l : list) {
        for (const auto& value : l) {
            data[i][j] = value;
            j++;
        }
        i++;
        j = 0;
    }
}

template<typename T>
int Matrix<T>::getRows() {
    return this->rows;
}

template<typename T>
int Matrix<T>::getColumns() {
    return this->columns;
}

template<typename T>
Matrix<double> Matrix<T>::getInversed() {
    // !!! Only 3x3 !!!

    Matrix<double> result(this->rows);
    double determinant = 0;

    for (int i = 0; i < this->rows; i++)
        determinant = determinant + (data[0][i] * (data[1][(i + 1) % 3] * data[2][(i + 2) % 3] - \
                                                   data[1][(i + 2) % 3] * data[2][(i + 1) % 3]));

    for (int i = 0; i < this->rows; i++) {
        for (int j = 0; j < this->columns; j++)
            result(i, j) = ((data[(j + 1) % 3][(i + 1) % 3] * data[(j + 2) % 3][(i + 2) % 3]) - \
                            (data[(j + 1) % 3][(i + 2) % 3] * data[(j + 2) % 3][(i + 1) % 3])) / determinant;
    }

    return result;
}

template<typename T>
void Matrix<T>::print() {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            std::cout << data[i][j] << " ";
        }
        std::cout << std::endl;
    }
}

template<typename T>
const Vector<T> Matrix<T>::operator*(const Vector<T>& other) const {
    Vector<T> result(this->columns);

    for (int i = 0; i < rows; i++) {
        result[i] = 0;
        for (int j = 0; j < columns; j++) {
            result[i] += data[i][j] * other[j];
        }
    }

    return result;
}

template<typename T>
int Matrix<T>::getRows() const {
    return this->rows;
}

template<typename T>
int Matrix<T>::getColumns() const {
    return this->columns;
}

#endif // COURSEPROJECT_MATRIX_H