//
// Created by Marseille Gulaya on 26/11/2018.
//

#ifndef COURSEPROJECT_VECTOR_H
#define COURSEPROJECT_VECTOR_H

#include <cmath>
#include <initializer_list>
#include "Matrix.h"

template<typename T>
class Vector {
private:
    int capacity;
    int size;
    T* data;

    void initialize(int from);

public:
    Vector();
    explicit Vector(int size);
    Vector(const Vector& other);
    Vector(std::initializer_list<T> list);

    ~Vector();

    T& operator[](int index);
    const T& operator[](int index) const;
    Vector<T>& operator=(const Vector& other);
    Vector<T> operator+(const Vector& other);
    Vector<T> operator-(const Vector& other);
    Vector<T>& operator+=(const Vector& other);
    Vector<T>& operator-=(const Vector& other);
    Vector<T> operator*(const Vector& other); // Умножение векторов
    Vector<T> operator*(const Matrix<double>& matrix); // Умножение на матрицу
    Vector<T> operator*(const T value); // Умножение вектора на число
    Vector<T>& operator*=(const Vector& other);
    Vector<T>& operator*=(const T value);
    bool operator==(const Vector& other);
    bool operator!=(const Vector& other);
    const double operator^(const Vector &other); // Угол между векторами

    int getSize();
    void expand();
    void pushBack(const T& element);
    void normalize() const;
    Vector<T> cross(const Vector<T>& other);
    double dot(const Vector<T>& other);
};

template<typename T>
Vector<T>::Vector() {
    this->capacity = 0;
    this->size = 0;

    this->data = nullptr;
}

template<typename T>
Vector<T>::Vector(int size) {
    this->capacity = size;
    this->size = size;

    this->data = new T[this->capacity];

    initialize(0);
}

template<typename T>
Vector<T>::Vector(const Vector& other) {
    this->capacity = other.capacity;
    this->size = other.size;
    this->data = new T[other.capacity];

    for (int i = 0; i < size; i++) {
        data[i] = other.data[i];
    }
}

template<typename T>
Vector<T>::~Vector() {
    delete[] data;
}

template<typename T>
int Vector<T>::getSize() {
    return this->size;
}

template<typename T>
void Vector<T>::expand() {
    this->capacity = (this->capacity + 1) * 2;

    T* tempData = new T[this->capacity];

    for (int i = 0; i < this->size; i++) {
        tempData[i] = this->data[i];
    }

    delete[] this->data;
    this->data = tempData;
}

template<typename T>
T& Vector<T>::operator[](int index) {
    return this->data[index];
}

template<typename T>
const T& Vector<T>::operator[](int index) const {
    return this->data[index];
}

template<typename T>
Vector<T>& Vector<T>::operator=(const Vector& other) {
    if (this->data) {
        delete[] this->data;
    }

    this->size = other.size;
    this->capacity = other.capacity;
    this->data = new T[other.capacity];

    for (int i = 0; i < other.size; i++) {
        this->data[i] = other.data[i];
    }

    return *this;
}

template<typename T>
void Vector<T>::pushBack(const T& element) {
    if (this->size >= this->capacity) {
        expand();
    }

    this->data[this->size++] = element;

    initialize(this->size + 1);
}

template<typename T>
void Vector<T>::initialize(int from) {
    for (int i = from; i < this->capacity; i++) {
        this->data[i] = T();
    }
}

template<typename T>
Vector<T>& Vector<T>::operator+=(const Vector& other) {
    if (this->size != other.size) {
        throw "Sizes are not equal.";
    }

    for (int i = 0; i < this->size; i++) {
        this->data[i] += other.data[i];
    }

    return *this;
}

template<typename T>
Vector<T>& Vector<T>::operator-=(const Vector& other) {
    if (this->size != other.size) {
        throw "Sizes are not equal.";
    }

    for (int i = 0; i < this->size; i++) {
        this->data[i] -= other.data[i];
    }

    return *this;
}

template<typename T>
Vector<T> Vector<T>::operator+(const Vector& other) {
    if (this->size != other.size) {
        throw "Sizes are not equal.";
    }

    Vector<T> temp(this->size);

    for (int i = 0; i < this->size; i++) {
        temp.data[i] = this->data[i] + other.data[i];
    }

    return temp;
}

template<typename T>
Vector<T> Vector<T>::operator-(const Vector& other) {
    if (this->size != other.size) {
        throw "Sizes are not equal.";
    }

    Vector<T> temp(this->size);

    for (int i = 0; i < this->size; i++) {
        temp.data[i] = this->data[i] - other.data[i];
    }

    return temp;
}

template<typename T>
Vector<T> Vector<T>::operator*(const Vector& other) {
    if (this->size != other.size) {
        throw "Sizes are not equal.";
    }

    Vector<T> temp(this->size);

    for (int i = 0; i < this->size; i++) {
        temp.data[i] = this->data[i] * other.data[i];
    }

    return temp;
}

template<typename T>
Vector<T> Vector<T>::operator*(const T value) {
    Vector<T> temp(this->size);

    for (int i = 0; i < this->size; i++) {
        temp.data[i] = this->data[i] * value;
    }

    return temp;
}

template<typename T>
Vector<T>& Vector<T>::operator*=(const Vector& other) {
    if (this->size != other.size) {
        throw "Sizes are not equal.";
    }

    for (int i = 0; i < this->size; i++) {
        this->data[i] *= other.data[i];
    }

    return *this;
}

template<typename T>
Vector<T>& Vector<T>::operator*=(const T value) {
    for (int i = 0; i < this->size; i++) {
        this->data[i] *= value;
    }

    return *this;
}

template<typename T>
bool Vector<T>::operator==(const Vector& other) {
    bool flag = true;

    if (this->size == other.size) {
        for (int i = 0; i < this->size && flag; i++) {
            if (this->data[i] != other.data[i]) {
                flag = false;
            }
        }
    } else {
        flag = false;
    }

    return flag;
}

template<typename T>
bool Vector<T>::operator!=(const Vector& other) {
    bool flag = false;

    if (this->size == other.size) {
        for (int i = 0; i < this->size && !flag; i++) {
            if (this->data[i] != other.data[i]) {
                flag = true;
            }
        }
    } else {
        flag = true;
    }

    return flag;
}

template<typename T>
void Vector<T>::normalize() const {
    double abs_value = 0;

    for (int i = 0; i < size; i++) {
        abs_value += pow(data[i], 2);
    }

    for (int i = 0; i < size; i++) {
        data[i] /= sqrt(abs_value);
    }
}

template<typename T>
Vector<T>::Vector(std::initializer_list<T> list) {
    this->size = list.size();
    this->capacity = list.size();
    this->data = new T[this->capacity];

    const T* begin = list.begin();
    const T* end = list.end();
    const T* current = begin;

    for (int i = 0; current != end; current++, i++) {
        this->data[i] = *current;
    }
}

template<typename T>
Vector<T> Vector<T>::operator*(const Matrix<double>& matrix) {
    Vector<T> tempVector(matrix.getColumns());

    for (int i = 0; i < tempVector.getSize(); i++) {
        tempVector[i] = 0;

        for (int j = 0; j < matrix.getRows(); j++) {
            tempVector[i] += matrix(j, i) * this->data[j];
        }
    }

    return tempVector;
}

template<typename T>
Vector<T> Vector<T>::cross(const Vector<T>& other) {
    Vector<T> tempVector;

    tempVector.pushBack(this->data[1] * other.data[2] - this->data[2] * other.data[1]);
    tempVector.pushBack(this->data[0] * other.data[2] - this->data[2] * other.data[0]);
    tempVector.pushBack(this->data[0] * other.data[1] - this->data[1] * other.data[0]);

    return tempVector;
}

template<typename T>
double Vector<T>::dot(const Vector<T>& other) {
    if (this->size != other.size) {
        throw "Not equal sizes.";
    }

    double result = 0;

    for (int i = 0; i < this->size; i++) {
        result += this->data[i] * other.data[i];
    }

    return result;
}

template<typename T>
const double Vector<T>::operator^(const Vector& other) {
    if ((data[0] == 0 && data[1] == 0 && data[2] == 0) || (other.data[0] == 0 && other.data[1] == 0 && other.data[2] == 0)) return 0;

//    normalize();
//    other.normalize();

    double result = 0;
    for (int i = 0; i < 3; i++) {
        result += this->data[i] * other.data[i];
    }

    double v1 = 0, v2 = 0;
    for (int i = 0; i < 3; i++) {
        v1 += this->data[i] * this->data[i];
        v2 += other.data[i] * other.data[i];
    }

    result /= sqrt(v1) * sqrt(v2);

    if (fabs(result) > 1) {
        result = 1 * result / fabs(result);
    }

    return acos(result);
}


#endif //COURSEPROJECT_VECTOR_H
