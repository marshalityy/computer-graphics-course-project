//
// Created by Marseille Gulaya on 27/11/2018.
//

#include "ConcreteObject.h"
#include <iostream>

void ConcreteObject::setVertices(Vector<Point> vertices)
{
    this->vertices = vertices;
}

void ConcreteObject::setFaces(Vector<Face> faces)
{
    this->faces = faces;
}

void ConcreteObject::setTransformMatrix(TransformMatrix transformMatrix)
{
    this->transformMatrix = transformMatrix;
}

ConcreteObject::ConcreteObject(Vector<Point> vertices, Vector<Face> faces)
{
    this->vertices = vertices;
    this->faces = faces;
}

void ConcreteObject::draw(double** zbuffer, ObjectDrawer& drawer)
{
    Vector<double> vertex(4);
    Vector<Point> temp = vertices;

    for (int i = 0; i < vertices.getSize(); i++)
    {
        vertex = formVector(vertices[i]);
        vertex = vertex * transformMatrix.getCurrentState();
        temp[i] = Point(vertex[0], vertex[1], vertex[2]);
    }

    drawer.drawFaces(zbuffer, temp, this->faces);
}

void ConcreteObject::translate(double dx, double dy, double dz)
{
    transformMatrix.updateByTranslation(dx, dy, dz);
}

void ConcreteObject::scale(double k)
{
    transformMatrix.updateByScaling(k);
}

void ConcreteObject::rotate(double xa, double ya, double za)
{
    transformMatrix.updateByRotation(xa, ya, za);
}

Vector<double> ConcreteObject::formVector(Point point)
{
    Vector<double> vertex(4);

    vertex[0] = point.getX();
    vertex[1] = point.getY();
    vertex[2] = point.getZ();
    vertex[3] = 1;

    return vertex;
}

//void ConcreteObject::reviewMode(ObjectDrawer& drawer)
//{
//    if (this->vertices.getSize() == 0 || this->faces.getSize() == 0) return;
//
//    this->reviewFlag = !this->reviewFlag;
//
//    while (this->reviewFlag)
//    {
//        transformMatrix.updateByRotation(0.1, 0.1, 0.1);
//        this->draw(drawer);
//        QCoreApplication::processEvents();
//        drawer.sceneClear();
//    }
//}

void ConcreteObject::faceObjectTo(ConcreteObject otherObject)
{
    /*
    Vector<double> center = otherObject.getCenter();
    Vector<double> eye = this->getCenter();
    Vector<double> direction = center - eye;
    Vector<double> forward = this->getDirection();

    Vector<double> xProjection = direction;
    xProjection[0] = 0;
    Vector<double> yProjection = direction;
    yProjection[1] = 0;
    Vector<double> zProjection = direction;
    zProjection[2] = 0;

    Vector<double> xForward = forward;
    xForward[0] = 0;
    Vector<double> yForward = forward;
    yForward[1] = 0;
    Vector<double> zForward = forward;
    zForward[2] = 0;

    double angleX = xProjection ^xForward;
    double angleY = yProjection ^yForward;
    double angleZ = zProjection ^zForward;

    transformMatrix.updateByRotation(angleX, angleY, angleZ);
     */

    /*
    Сделать матрицу поворота по Z, затем умножить эту матрица на camera-to-world матрицу
    Серая пометка в статье
    Сделать проекцию на ось Z, как в коде выше, построить матрицу поворота
     */

    Vector<double> temp = {0, 1, 0};

    Vector<double> center = this->getCenter();

    Vector<double> forward = otherObject.getCenter() - center;
    forward.normalize();

    Vector<double> right = temp.cross(forward);
    right.normalize();

    Vector<double> up = forward.cross(right);

    Matrix<double> newMatrix = this->getTransformMatrix().getCurrentState();

    newMatrix(0, 0) = right[0];
    newMatrix(0, 1) = right[1];
    newMatrix(0, 2) = right[2];

    newMatrix(1, 0) = up[0];
    newMatrix(1, 1) = up[1];
    newMatrix(1, 2) = up[2];

    newMatrix(2, 0) = forward[0];
    newMatrix(2, 1) = forward[1];
    newMatrix(2, 2) = forward[2];

    TransformMatrix newObjectMatrix(newMatrix);
    newObjectMatrix.getCurrentState().print();
    this->setTransformMatrix(newObjectMatrix);
}

TransformMatrix ConcreteObject::getTransformMatrix()
{
    return this->transformMatrix;
}

Vector<double> ConcreteObject::getCenter()
{
    Matrix<double> temp = this->transformMatrix.getCurrentState();
    Vector<double> center;

    for (int i = 0; i < temp.getRows() - 1; i++)
    {
        center.pushBack(temp(3, i));
    }

    return center;
}

Vector<double> ConcreteObject::getDirection()
{
    Matrix<double> temp = this->transformMatrix.getCurrentState();
    Vector<double> forward;

    for (int i = 0; i < temp.getRows() - 1; i++)
    {
        forward.pushBack(temp(2, i));
    }

    return forward;
}
