//
// Created by Marseille Gulaya on 29/11/2018.
//

#ifndef COURSEPROJECT_ABSTRACTOBJECT_H
#define COURSEPROJECT_ABSTRACTOBJECT_H

#include "templates/Matrix.h"
#include "point/Point.h"
#include "../face/Face.h"
#include "../drawer/ObjectDrawer.h"
#include "../transform/TransformMatrix.h"

class AbstractObject {
protected:
    Vector<Point> vertices;
    Vector<Face> faces;
    TransformMatrix transformMatrix;

public:
    AbstractObject() = default;
    virtual ~AbstractObject() = default;;

    virtual void rotate(double xa, double ya, double za) = 0;
    virtual void translate(double dx, double dy, double dz) = 0;
    virtual void scale(double k) = 0;

    virtual void setVertices(Vector<Point> vertices) = 0;
    virtual void setFaces(Vector<Face> faces) = 0;
    virtual void setTransformMatrix(TransformMatrix transformMatrix) = 0;
    virtual TransformMatrix getTransformMatrix() = 0;

    virtual void draw(double** zbuffer, ObjectDrawer& drawer) = 0;
};


#endif //COURSEPROJECT_ABSTRACTOBJECT_H
