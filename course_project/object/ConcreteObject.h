//
// Created by Marseille Gulaya on 24/11/2018.
//

#ifndef COURSEPROJECT_BASEMODEL_H
#define COURSEPROJECT_BASEMODEL_H

#include "AbstractObject.h"
#include <QCoreApplication>

class ConcreteObject : public AbstractObject {
private:
    bool reviewFlag = false;

    Vector<double> formVector(Point point);

public:
    ConcreteObject() = default;
    ConcreteObject(Vector<Point> vertices, Vector<Face> faces);

    ~ConcreteObject() override = default;

    void rotate(double xa, double ya, double za) override;
    void translate(double dx, double dy, double dz) override;
    void scale(double k) override;

    void setVertices(Vector<Point> vertices) override;
    void setFaces(Vector<Face> faces) override;
    void setTransformMatrix(TransformMatrix transformMatrix) override;
    Vector<double> getCenter();
    Vector<double> getDirection();
    TransformMatrix getTransformMatrix() override;

    void draw(double** zbuffer, ObjectDrawer& drawer) override;
//    void reviewMode(ObjectDrawer& drawer);

    void faceObjectTo(ConcreteObject otherObject);
};

#endif //COURSEPROJECT_BASEMODEL_H
