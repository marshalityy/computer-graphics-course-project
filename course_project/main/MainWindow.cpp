//
// Created by Marseille Gulaya on 18/11/2018.
//

#include "MainWindow.h"
#include <iostream>

#define SCREEN_WIDTH 1000

mainwidget::mainwidget(QWidget* parent) :
        QMainWindow(parent),
        ui(new Ui::mainwidget) {
    ui->setupUi(this);

    scene = new QGraphicsScene(this);

    this->zbuffer = (double**)malloc(sizeof(double*) * width());
    for (int i = 0; i < width(); i++) {
        this->zbuffer[i] = (double*) calloc(height(), sizeof(double));
    }

    drawer = ObjectDrawer(scene);

    ui->graphicsView->centerOn(0, 0);

    ui->graphicsView->setScene(scene);
    ui->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    scene->setSceneRect(-SCREEN_WIDTH, -SCREEN_WIDTH, SCREEN_WIDTH * 2, SCREEN_WIDTH * 2);
}

mainwidget::~mainwidget() {
    for (int i = 0; i < width(); i++) {
        free(zbuffer[i]);
    }
    free(zbuffer);

    delete ui;
}

void mainwidget::on_loadButton_clicked() {
    QString objName = QFileDialog::getOpenFileName(this);
    if (objName.isEmpty()) return;

    QByteArray byteArray = objName.toLatin1();
    const char* fileName = byteArray.data();

    QString string = "Object №" + QString::number(ui->selectObject->count() + 1);
    ui->selectObject->addItem(string);

    receiver.loadObject(fileName);
    receiver.setActiveObject(ui->selectObject->count() - 1);
    receiver.drawObjects(zbuffer, scene);

    ui->selectObject->setCurrentIndex(ui->selectObject->count() - 1);
}

void mainwidget::on_rotateButton_clicked() {
    double xa = ui->rx_input->text().toDouble();
    double ya = ui->ry_input->text().toDouble();
    double za = ui->rz_input->text().toDouble();

    receiver.rotateObject(xa, ya, za);
    scene->clear();
    receiver.drawObjects(zbuffer, scene);
}

void mainwidget::on_scaleButton_clicked() {
    double k = ui->scale_input->text().toDouble();

    receiver.scaleObject(k);
    scene->clear();
    receiver.drawObjects(zbuffer, scene);
}

void mainwidget::on_translateButton_clicked() {
    double dx = ui->dx_input->text().toDouble();
    double dy = ui->dy_input->text().toDouble();
    double dz = ui->dz_input->text().toDouble();

    receiver.translateObject(dx, dy, dz);
    scene->clear();
    receiver.drawObjects(zbuffer, scene);
}

void mainwidget::on_reviewButton_clicked() {
//    receiver.reviewMode(scene);
    receiver.faceObject();
    scene->clear();
    receiver.drawObjects(zbuffer, scene);
}

void mainwidget::on_exitButton_clicked() {
    QCoreApplication::exit(0);
}

void mainwidget::on_setActiveObjBtn_clicked() {
    this->receiver.setActiveObject(ui->selectObject->currentIndex());
    scene->clear();
    receiver.drawObjects(zbuffer, scene);
}
