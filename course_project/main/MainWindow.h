//
// Created by Marseille Gulaya on 18/11/2018.
//

#ifndef COURSEPROJECT_MAINWINDOW_H
#define COURSEPROJECT_MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QFileDialog>
#include "drawer/ObjectDrawer.h"
#include "object/ConcreteObject.h"
#include "../Receiver.h"
#include "./cmake-build-debug/ui_mainwindow.h"
#include "parser/ObjectParser.h"
#include <QCoreApplication>
#include <QComboBox>
#include <string>
#include "interpolator/Interpolator.h"

namespace Ui {
    class mainwidget;
}

class mainwidget : public QMainWindow
{
Q_OBJECT

public:
    explicit mainwidget(QWidget *parent = nullptr);
    ~mainwidget();

private slots:
    void on_loadButton_clicked();
    void on_rotateButton_clicked();
    void on_scaleButton_clicked();
    void on_translateButton_clicked();
    void on_setActiveObjBtn_clicked();
    void on_reviewButton_clicked();
    void on_exitButton_clicked();

private:
    Ui::mainwidget *ui;
    QGraphicsScene* scene;
    Receiver receiver;
    ObjectDrawer drawer;
    double** zbuffer;
};

#endif //COURSEPROJECT_MAINWINDOW_H
