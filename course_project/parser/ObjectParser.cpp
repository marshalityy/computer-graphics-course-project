//
// Created by Marseille Gulaya on 27/11/2018.
//

#include "ObjectParser.h"
#include <iostream>

ObjectParser::ObjectParser(const char* fileName) {
    file = std::ifstream(fileName);
}

void ObjectParser::parse(AbstractObject& object) {
    if (!file) {
        throw "Can't open file.";
    }

    std::string line;
    Vector<Point> vertices;
    Vector<Face> faces;

    while (std::getline(file, line))
    {
        //check v for vertices
        if (line.substr(0, 2) == "v ")
        {
            std::istringstream vLine(line.substr(2));
            Point vertex;
            double x, y, z;

            vLine >> x;
            vLine >> y;
            vLine >> z;

            vertex = Point(x, y, z);
            vertices.pushBack(vertex);
        }
            //check for faces
        else if(line.substr(0, 2) == "f ")
        {
            std::istringstream fLine(line.substr(2));
            Face face;
            int a, b, c;

            fLine >> a;
            fLine >> b;
            fLine >> c;

            a--; b--; c--;

            face = Face(a, b, c);
            faces.pushBack(face);
        }
    }

    object.setFaces(faces);
    object.setVertices(vertices);
}

