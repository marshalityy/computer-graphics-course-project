//
// Created by Marseille Gulaya on 27/11/2018.
//

#ifndef COURSEPROJECT_FILEPARSER_H
#define COURSEPROJECT_FILEPARSER_H

#include <sstream>
#include <fstream>
#include <string>
#include "object/ConcreteObject.h"

class ObjectParser {
public:
    ObjectParser() = default;
    explicit ObjectParser(const char* fileName);

    void parse(AbstractObject& object);

private:
    std::ifstream file;
};


#endif //COURSEPROJECT_FILEPARSER_H
