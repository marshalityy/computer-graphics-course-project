//
// Created by Marseille Gulaya on 27/11/2018.
//

#include "Face.h"

Face::Face(int a, int b, int c) {
    this->a = a;
    this->b = b;
    this->c = c;
}

int Face::getA() const {
    return this->a;
}

void Face::setA(int a) {
    this->a = a;
}

int Face::getB() const {
    return this->b;
}

void Face::setB(int b) {
    this->b = b;
}

int Face::getC() const {
    return this->c;
}

void Face::setC(int c) {
    this->c = c;
}
