//
// Created by Marseille Gulaya on 27/11/2018.
//

#ifndef COURSEPROJECT_FACE_H
#define COURSEPROJECT_FACE_H


class Face {
private:
    int a, b, c;

public:
    Face() = default;
    ~Face() = default;

    explicit Face(int a, int b, int c);

    int getA() const;
    void setA(int a);

    int getB() const;
    void setB(int b);

    int getC() const;
    void setC(int c);
};


#endif //COURSEPROJECT_FACE_H
