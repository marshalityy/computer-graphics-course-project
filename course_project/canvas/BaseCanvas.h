//
// Created by Marseille Gulaya on 2018-12-18.
//

#ifndef COURSEPROJECT_BASECANVAS_H
#define COURSEPROJECT_BASECANVAS_H

class BaseCanvas {
public:
    virtual ~BaseCanvas() = default;

    virtual void drawLine(double x1, double y1, double x2, double y2) = 0;
    virtual void sceneClear() = 0;
};

#endif //COURSEPROJECT_BASECANVAS_H
