//
// Created by Marseille Gulaya on 22/11/2018.
//

#include "Interpolator.h"

void Interpolator::getArrays() {
    for (int i = 0; i < vertices.getSize(); i++) {
        this->xArray.pushBack(vertices[i].getX());
        this->yArray.pushBack(vertices[i].getY());
        this->zArray.pushBack(vertices[i].getZ());
    }
}

Interpolator::Interpolator(Vector<Point> vertices) {
    this->vertices = vertices;
    getArrays();
}

double Interpolator::getPolynom(Vector<double> diffs, Vector<double> first, Vector<double> second, double node) {
    double temp = 1;
    double polynom = second[0];

    for (int i = 1; i < diffs.getSize(); i++) {
        temp = temp * (node - first[i - 1]);
        polynom += temp * diffs[i];
    }

    return polynom;
}

Vector<double> Interpolator::getDividedDiffs(Vector<double> first, Vector<double> second) {
    Vector<double> diffs = second;

    for (int j = 1; j < second.getSize(); j++) {
        for (int i = second.getSize() - 1; i > j - 1; i--) {
            diffs[i] = (diffs[i] - diffs[i - 1]) / (first[i] - first[i - j]);
        }
    }

    return diffs;
}

Vector<double> Interpolator::interpolateXY() {
    Vector<double> resultArray;
    Vector<double> diffs = getDividedDiffs(xArray, yArray);
    double value;

    for (int i = vertices[1].getX() + 1; i < vertices[2].getX(); i++) {
        value = getPolynom(diffs, xArray, yArray, i);
        resultArray.pushBack(value);
    }

    return resultArray;
}
