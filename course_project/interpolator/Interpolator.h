//
// Created by Marseille Gulaya on 22/11/2018.
//

#ifndef COURSEPROJECT_INTREPOLATOR_H
#define COURSEPROJECT_INTREPOLATOR_H

#include <vector>
#include "point/Point.h"
#include <templates/Vector.h>

class Interpolator {
private:
    Vector<Point> vertices;

    Vector<double> xArray;
    Vector<double> yArray;
    Vector<double> zArray;

    void getArrays();

    Vector<double> getDividedDiffs(Vector<double> first, Vector<double> second);
    double getPolynom(Vector<double> diffs, Vector<double> first, Vector<double> second, double node);

public:
    Interpolator() = default;

    explicit Interpolator(Vector<Point> Vertexs);

    Vector<double> interpolateXY();
//    Vector<double> interpolateYZ();
};


#endif //COURSEPROJECT_INTREPOLATOR_H
