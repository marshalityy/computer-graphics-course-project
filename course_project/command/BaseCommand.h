//
// Created by Marseille Gulaya on 2018-12-18.
//

#ifndef COURSEPROJECT_BASECOMMAND_H
#define COURSEPROJECT_BASECOMMAND_H

#include "../object/AbstractObject.h"
#include "../parser/ObjectParser.h"

class BaseCommand {
public:
    virtual ~BaseCommand() = default;
    virtual void execute() = 0;
};

#endif //COURSEPROJECT_BASECOMMAND_H
