//
// Created by Marseille Gulaya on 2018-12-18.
//

#ifndef COURSEPROJECT_COMMANDS_H
#define COURSEPROJECT_COMMANDS_H

#include "BaseCommand.h"
#include "../parser/ObjectParser.h"
#include "../drawer/ObjectDrawer.h"

class LoadObjCmd : public BaseCommand {
private:
    ObjectParser* parser;
    AbstractObject* object;

public:
    LoadObjCmd(AbstractObject* _object, ObjectParser* parser) {
        this->object = _object;
        this->parser = parser;
    }

    void execute() override { parser->parse(*object); }
};

class DrawObjCmd : public BaseCommand {
private:
    Vector<ConcreteObject*> objects;
    ObjectDrawer* drawer;
    double** zbuffer;
    QGraphicsScene* scene;
    int active;

public:
    DrawObjCmd(Vector<ConcreteObject*> _objects, int activeObj, double** _zbuffer, QGraphicsScene* _scene) : scene(_scene) {
        objects = _objects;
        zbuffer = _zbuffer;
        drawer = new ObjectDrawer(scene);
        active = activeObj;
    }

    void execute() override {
        for (int i = 0; i < objects.getSize(); i++) {
            if (i == active) {
                drawer->setColor(QPen(Qt::darkGreen));
            } else {
                drawer->setColor(QPen(Qt::darkGray));
            }
            objects[i]->draw(zbuffer, *drawer);
        }
    }
};

//class ReviewModeCmd : public BaseCommand {
//private:
//    ConcreteObject* object;
//    ObjectDrawer* drawer;
//    QGraphicsScene* scene;
//
//public:
//    ReviewModeCmd(ConcreteObject* _object, QGraphicsScene* _scene) : scene(_scene) {
//        object = _object;
//        drawer = new ObjectDrawer(scene);
//    }
//
//    void execute() override {
//        object->reviewMode(*drawer);
//    }
//};

class RotateObjCmd : public BaseCommand {
private:
    ConcreteObject* object;
    double xa, ya, za;

public:
    RotateObjCmd(ConcreteObject* _object, double _xa, double _ya, double _za) : xa(_xa), ya(_ya), za(_za) {
        object = _object;
    }

    void execute() override { object->rotate(xa, ya, za); }
};

class ScaleObjCmd : public BaseCommand {
private:
    ConcreteObject* object;
    double k;

public:
    ScaleObjCmd(ConcreteObject* _object, double _k) : k(_k) {
        object = _object;
    }

    void execute() override { object->scale(k); }
};

class TranslateObjCmd : public BaseCommand {
private:
    ConcreteObject* object;
    double dx, dy, dz;

public:
    TranslateObjCmd(ConcreteObject* _object, double _dx, double _dy, double _dz) : dx(_dx), dy(_dy), dz(_dz) {
        object = _object;
    }

    void execute() override { object->translate(dx, dy, dz); }
};

#endif //COURSEPROJECT_COMMANDS_H
