//
// Created by Marseille Gulaya on 2018-12-18.
//

#ifndef COURSEPROJECT_RECEIVER_H
#define COURSEPROJECT_RECEIVER_H

#include "object/ConcreteObject.h"
#include "parser/ObjectParser.h"
#include "command/Commands.h"

class Receiver {
private:
    int activeObj = -1;
    int selected = 1;
    Vector<ConcreteObject*> objects;
    BaseCommand* command;

public:
    Receiver() = default;

    ~Receiver() {
        for (int i = 0; i < objects.getSize(); i++) {
            delete objects[i];
        }
    }

    void loadObject(const char* filename) {
        ConcreteObject* object = new ConcreteObject();
        command = new LoadObjCmd(object, new ObjectParser(filename));
        command->execute();
        objects.pushBack(object);
        delete command;
    }

    void drawObjects(double** zbuffer, QGraphicsScene* scene) {
        command = new DrawObjCmd(objects, activeObj, zbuffer, scene);
        command->execute();
        delete command;
    }

    void rotateObject(double xa, double ya, double za) {
        command = new RotateObjCmd(objects[activeObj], xa, ya, za);
        command->execute();
        delete command;
    }

    void scaleObject(double k) {
        command = new ScaleObjCmd(objects[activeObj], k);
        command->execute();
        delete command;
    }

    void translateObject(double dx, double dy, double dz) {
        command = new TranslateObjCmd(objects[activeObj], dx, dy, dz);
        command->execute();
        delete command;
    }

//    void reviewMode(QGraphicsScene* scene) {
//        if (activeObj > -1) {
//            command = new ReviewModeCmd(objects[activeObj], scene);
//            command->execute();
//            delete command;
//        }
//    }

    void faceObject() {
        objects[0]->faceObjectTo(*objects[selected]);
    }

    void setActiveObject(int index) {
        this->activeObj = index;
    }
};

#endif //COURSEPROJECT_RECEIVER_H
