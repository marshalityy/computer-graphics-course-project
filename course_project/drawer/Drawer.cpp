//
// Created by Marseille Gulaya on 26/11/2018.
//

#include "Drawer.h"

QRectF Drawer::boundingRect() const {
    return QRectF(0, 0, 0, 0);
}

void Drawer::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) {
    Q_UNUSED(option);
    Q_UNUSED(widget);
    painter->setPen(color);
    painter->drawPoint(x, y);
}

void Drawer::setX(int x) {
    this->x = x;
}

void Drawer::setY(int y) {
    this->y = y;
}

void Drawer::setColor(int r, int g, int b) {
    QPen pen = QPen(QColor(r, g, b));
    this->color = pen;
}

