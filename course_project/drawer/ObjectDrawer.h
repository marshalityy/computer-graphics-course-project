//
// Created by Marseille Gulaya on 29/11/2018.
//

#ifndef COURSEPROJECT_OBJECTDRAWER_H
#define COURSEPROJECT_OBJECTDRAWER_H

#include <math.h>
#include <limits>
#include <stdlib.h>
#include "Drawer.h"
#include "point/Point.h"
#include "face/Face.h"
#include "templates/Matrix.h"
#include <templates/Vector.h>

struct light {
    Point C;
    double I;
};

class ObjectDrawer {
private:
    QGraphicsScene* scene;

    QPen color;

//    void drawPoint(double x, double y);
    void drawPoint(double x, double y, int r, int g, int b);
    void drawLine(Point start, Point end);

public:
    void drawPoint(double x, double y);
    ObjectDrawer() = default;
    explicit ObjectDrawer(QGraphicsScene* scene);

    ~ObjectDrawer() = default;

    void drawFace(double** zbuffer, Vector<Point> vertices, Face face);
    void drawFaces(double** zbuffer, Vector<Point> vertices, Vector<Face> faces);
    void setColor(QPen color);

    void sceneClear();

    // FUCK
    bool isEqual(double x, double y);
    double cosinus(Point a, Point L);
    double dist(Point a, Point L);
    void AreaGuro(double** zbuffer, light L, double kd, Point a, Point b, Point c, int Cr, int Cg, int Cb);
    void PaintSideGuro(double** zbuffer, Vector<Point> side, int n, light L, double kd, int Cr, int Cg, int Cb);
};


#endif //COURSEPROJECT_OBJECTDRAWER_H
