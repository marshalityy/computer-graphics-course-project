//
// Created by Marseille Gulaya on 29/11/2018.
//

#include "ObjectDrawer.h"
#include "point/Point.h"

double ObjectDrawer::cosinus(Point a, Point L) {
    double coss = (a.x * L.x + a.y * L.y + a.z * L.z) /
                  (2 * sqrt(a.x * a.x + a.y * a.y + a.z * a.z) * sqrt(L.x * L.x + L.y * L.y + L.z * L.z));
    return coss;
}

double ObjectDrawer::dist(Point a, Point L) {
    double d = sqrt((a.x - L.x) * (a.x - L.x) + (a.y - L.y) * (a.y - L.y) + (a.z - L.z) * (a.z - L.z));
    return d;
}

//void ObjectDrawer::AreaGuro(double** zbuffer, light L, double kd, Point a, Point b, Point c, int Cr, int Cg, int Cb) {
//    double Ia, Ib, Ic;
//    double I1, I2, I;
//    double y, x, z;
//    double x1, x2, z1, z2, t1, t2, t;
//    int R, G, B;
//
//    const double K = 1;
//
//    if (a == b && a == c) {
//        return;
//    }
//
//    Point temp;
//
//    if (a.y > b.y) {
//        temp = a;
//        a = b;
//        b = temp;
//    }
//
//    if (b.y > c.y) {
//        temp = b;
//        b = c;
//        c = temp;
//    }
//
//    if (a.y > b.y) {
//        temp = a;
//        a = b;
//        b = temp;
//    }
//
//    Ia = (L.I * kd * cosinus(a, L.C) / (dist(a, L.C) + K)) * 100;
//    Ib = (L.I * kd * cosinus(b, L.C) / (dist(b, L.C) + K)) * 100;
//    Ic = (L.I * kd * cosinus(c, L.C) / (dist(c, L.C) + K)) * 100;
//
//
//    for (y = a.y; y <= b.y; y++) {
//        if (a.x == b.x) {
//            b.x--;
//        }
//
//        if (a.x == c.x) {
//            c.x--;
//        }
//
//        x1 = ((y - a.y) / (b.y - a.y)) * (b.x - a.x) + a.x;
//        x2 = ((y - a.y) / (c.y - a.y)) * (c.x - a.x) + a.x;
//
//        z1 = ((y - a.y) / (b.y - a.y)) * (b.z - a.z) + a.z;
//        z2 = ((y - a.y) / (c.y - a.y)) * (c.z - a.z) + a.z;
//
//        t1 = (x1 - b.x) / (a.x - b.x);
//        t2 = (x2 - c.x) / (a.x - c.x);
//        I1 = t1 * Ia + (1 - t1) * Ib;
//        I2 = t2 * Ia + (1 - t2) * Ic;
//
//        if (x2 == x1) {
//            continue;
//        }
//
//        if (x2 > x1) {
//            for (x = std::round(x1); x <= std::round(x2); x++) {
//                z = z1 + (z2 - z1) * ((x - x1) / (x2 - x1));
//
//                t = (x2 - x) / (x2 - x1);
//                I = t * I1 + (1 - t) * I2;
//
//                if (I > 1)I = 1;
//                if (I < 0)I = 0;
//                R = int(Cr * I);
//                G = int(Cg * I);
//                B = int(Cb * I);
//                if (R > 255)R = 255;
//                if (R < 0)R = 0;
//                if (G > 255)G = 255;
//                if (G < 0)G = 0;
//                if (B > 255)B = 255;
//                if (B < 0)B = 0;
//
//                if (x >= 0 & y >= 0 & x <= 1255 & y <= 762) {
//                    if (zbuffer[int(std::round(x))][int(std::round(y))] <= z ||
//                        zbuffer[int(std::round(x))][int(std::round(y))] == 0) {
//                        drawPoint(int(std::round(x)), int(std::round(y)), R, G, B);
//                        zbuffer[int(std::round(x))][int(std::round(y))] = z;
//                    }
//                }
//            }
//        } else {
//            for (x = std::round(x1); x >= std::round(x2); x--) {
//                z = z2 + (z1 - z2) * ((x - x2) / (x1 - x2));
//
//                t = (x2 - x) / (x2 - x1);
//                I = t * I1 + (1 - t) * I2;
//
//                R = int(Cr * I);
//                G = int(Cg * I);
//                B = int(Cb * I);
//                if (R > 255)R = 255;
//                if (R < 0)R = 0;
//                if (G > 255)G = 255;
//                if (G < 0)G = 0;
//                if (B > 255)B = 255;
//                if (B < 0)B = 0;
//
//                if (x >= 0 & y >= 0 & x <= 1255 & y <= 762) {
//                    if (zbuffer[int(std::round(x))][int(std::round(y))] <= z ||
//                        zbuffer[int(std::round(x))][int(std::round(y))] == 0) {
//                        drawPoint(int(std::round(x)), int(std::round(y)), R, G, B);
//                        zbuffer[int(std::round(x))][int(std::round(y))] = z;
//                    }
//                }
//            }
//        }
//    }
//
//    for (y = b.y; y <= c.y; y++) {
//        if (c.x == b.x) {
//            b.x--;
//        }
//
//        if (c.x == a.x) {
//            a.x--;
//        }
//
//        x1 = ((y - b.y) / (c.y - b.y)) * (c.x - b.x) + b.x;
//        x2 = ((y - a.y) / (c.y - a.y)) * (c.x - a.x) + a.x;
//
//        z1 = ((y - b.y) / (c.y - b.y)) * (c.z - b.z) + b.z;
//        z2 = ((y - a.y) / (c.y - a.y)) * (c.z - a.z) + a.z;
//
//        t1 = (x1 - c.x) / (b.x - c.x);
//        t2 = (x2 - c.x) / (a.x - c.x);
//        I1 = t1 * Ib + (1 - t1) * Ic;
//        I2 = t2 * Ia + (1 - t2) * Ic;
//
//        if (x2 == x1) continue;
//
//        if (x2 > x1) {
//            for (x = std::round(x1); x <= std::round(x2); x++) {
//                z = z1 + (z2 - z1) * ((x - x1) / (x2 - x1));
//
//                t = (x2 - x) / (x2 - x1);
//                I = t * I1 + (1 - t) * I2;
//
//                R = int(Cr * I);
//                G = int(Cg * I);
//                B = int(Cb * I);
//                if (R > 255)R = 255;
//                if (R < 0)R = 0;
//                if (G > 255)G = 255;
//                if (G < 0)G = 0;
//                if (B > 255)B = 255;
//                if (B < 0)B = 0;
//
//                if (x >= 0 & y >= 0 & x <= 1255 & y <= 762) {
//                    if (zbuffer[int(std::round(x))][int(std::round(y))] <= z ||
//                        zbuffer[int(std::round(x))][int(std::round(y))] == 0) {
//                        drawPoint(int(std::round(x)), int(std::round(y)), R, G, B);
//                        zbuffer[int(std::round(x))][int(std::round(y))] = z;
//                    }
//                }
//            }
//        } else {
//            for (x = std::round(x1); x >= std::round(x2); x--) {
//                z = z2 + (z1 - z2) * ((x - x2) / (x1 - x2));
//
//                t = (x2 - x) / (x2 - x1);
//                I = t * I1 + (1 - t) * I2;
//
//                if (I > 1)I = 1;
//                if (I < 0)I = 0;
//                R = int(Cr * I);
//                G = int(Cg * I);
//                B = int(Cb * I);
//                if (R > 255)R = 255;
//                if (R < 0)R = 0;
//                if (G > 255)G = 255;
//                if (G < 0)G = 0;
//                if (B > 255)B = 255;
//                if (B < 0)B = 0;
//
//                if (x >= 0 & y >= 0 & x <= 1255 & y <= 762) {
//                    if (zbuffer[int(std::round(x))][int(std::round(y))] <= z ||
//                        zbuffer[int(std::round(x))][int(std::round(y))] == 0) {
//                        drawPoint(int(std::round(x)), int(std::round(y)), R, G, B);
//                        zbuffer[int(std::round(x))][int(std::round(y))] = z;
//                    }
//                }
//            }
//        }
//    }
//}

void ObjectDrawer::AreaGuro(double** zbuffer, light L, double kd, Point a, Point b, Point c, int Cr, int Cg, int Cb) {
    double Ia, Ib, Ic;
    double I1, I2, I;

    double y, x, z = 0;
    double x1, x2, z1, z2, t1, t2, t;

    int R, G, B;

    const double K = 1; //произвольная постоянная

    if (a == b && a == c) {
        return;
    }

    //упорядочить вершины по y
    Point temp;

    if (a.y > b.y) {
        temp = a;
        a = b;
        b = temp;
    }

    if (b.y > c.y) {
        temp = b;
        b = c;
        c = temp;
    }

    if (a.y > b.y) {
        temp = a;
        a = b;
        b = temp;
    }

    Ia = (L.I * kd * cosinus(a, L.C) / (dist(a, L.C) + K)) * 100;
    Ib = (L.I * kd * cosinus(b, L.C) / (dist(b, L.C) + K)) * 100;
    Ic = (L.I * kd * cosinus(c, L.C) / (dist(c, L.C) + K)) * 100;

    //закрашиваем линии от верхней к нижней
    for (y = a.y; y <= b.y; y++) {
        //интенсивности в точках пересечения линии(y) с границами грани
        if (isEqual(a.x, b.x)) {
            b.x--;
        }

        if (isEqual(a.x, c.x)) {
            c.x--;
        }

        //точки пересечения линии(y) с границами грани(ac,ab)
        x1 = ((y - a.y) / (b.y - a.y)) * (b.x - a.x) + a.x;
        x2 = ((y - a.y) / (c.y - a.y)) * (c.x - a.x) + a.x;

        z1 = ((y - a.y) / (b.y - a.y)) * (b.z - a.z) + a.z;
        z2 = ((y - a.y) / (c.y - a.y)) * (c.z - a.z) + a.z;

        t1 = (x1 - b.x) / (a.x - b.x);
        t2 = (x2 - c.x) / (a.x - c.x);

        I1 = t1 * Ia + (1 - t1) * Ib;
        I2 = t2 * Ia + (1 - t2) * Ic;

        if (isEqual(x2, x1)) {
            continue;
        }

        if (x2 > x1) {
            //рисовать линию
            for (x = std::round(x1); x <= std::round(x2); x++) {
                //интенсивность в точке x
                z = z1 + (z2 - z1) * ((x - x1) / (x2 - x1));
                t = (x2 - x) / (x2 - x1);
                I = t * I1 + (1 - t) * I2;

                if (I > 1) {
                    I = 1;
                }

                if (I < 0) {
                    I = 0;
                }

                //поставить точку
                R = int(Cr * I);
                G = int(Cg * I);
                B = int(Cb * I);

                if (R > 255) {
                    R = 255;
                }

                if (R < 0) {
                    R = 0;
                }

                if (G > 255) {
                    G = 255;
                }

                if (G < 0) {
                    G = 0;
                }

                if (B > 255) {
                    B = 255;
                }

                if (B < 0) {
                    B = 0;
                }

                if (x >= -625 && y >= -381 && x <= 652 && y <= 381) {
                    if (zbuffer[int(std::round(x)) + 625][int(std::round(y)) + 381] <= z || zbuffer[int(std::round(x)) + 625][int(std::round(y)) + 381] == 0) {
                        drawPoint(int(std::round(x)), int(std::round(y)), R, G, B);
                        zbuffer[int(std::round(x)) + 625][int(std::round(y)) + 381] = z;
                    }
                }
            }
        } else {
            //рисовать линию
            for (x = std::round(x1); x >= std::round(x2); x--) {
                //интенсивность в точке x
                z = z2 + (z1 - z2) * ((x - x2) / (x1 - x2));
                t = (x2 - x) / (x2 - x1);
                I = t * I1 + (1 - t) * I2;
                //поставить точку

                R = int(Cr * I);
                G = int(Cg * I);
                B = int(Cb * I);

                if (R > 255) {
                    R = 255;
                }

                if (R < 0) {
                    R = 0;
                }

                if (G > 255) {
                    G = 255;
                }

                if (G < 0) {
                    G = 0;
                }

                if (B > 255) {
                    B = 255;
                }

                if (B < 0) {
                    B = 0;
                }

                if (x >= -625 && y >= -381 && x <= 652 && y <= 381) {
                    if (zbuffer[int(std::round(x)) + 625][int(std::round(y)) + 381] <= z || zbuffer[int(std::round(x)) + 625][int(std::round(y)) + 381] == 0) {
                        drawPoint(int(std::round(x)), int(std::round(y)), R, G, B);
                        zbuffer[int(std::round(x)) + 625][int(std::round(y)) + 381] = z;
                    }
                }
            }
        }
    }

    for (y = b.y; y <= c.y; y++) {
        if (isEqual(c.x, b.x)) {
            b.x--;
        }

        if (isEqual(c.x, a.x)) {
            a.x--;
        }

        //точки пересечения линии(y) с границами грани(ac,bc)
        x1 = ((y - b.y) / (c.y - b.y)) * (c.x - b.x) + b.x;
        x2 = ((y - a.y) / (c.y - a.y)) * (c.x - a.x) + a.x;

        z1 = ((y - b.y) / (c.y - b.y)) * (c.z - b.z) + b.z;
        z2 = ((y - a.y) / (c.y - a.y)) * (c.z - a.z) + a.z;

        //интенсивности в точках пересечения линии(y) с границами грани

        t1 = (x1 - c.x) / (b.x - c.x);
        t2 = (x2 - c.x) / (a.x - c.x);

        I1 = t1 * Ib + (1 - t1) * Ic;
        I2 = t2 * Ia + (1 - t2) * Ic;

        if (isEqual(x2, x1)) {
            continue;
        }

        if (x2 > x1) {
            //рисовать линию
            for (x = std::round(x1); x <= std::round(x2); x++) {
                //интенсивность в точке x
                z = z1 + (z2 - z1) * ((x - x1) / (x2 - x1));
                t = (x2 - x) / (x2 - x1);
                I = t * I1 + (1 - t) * I2;
                //поставить точку

                if (I > 1) {
                    I = 1;
                }

                if (I < 0) {
                    I = 0;
                }

                R = int(Cr * I);
                G = int(Cg * I);
                B = int(Cb * I);

                if (R > 255) {
                    R = 255;
                }

                if (R < 0) {
                    R = 0;
                }

                if (G > 255) {
                    G = 255;
                }

                if (G < 0) {
                    G = 0;
                }

                if (B > 255) {
                    B = 255;
                }

                if (B < 0) {
                    B = 0;
                }

                if (x >= -625 && y >= -381 && x <= 652 && y <= 381) {
                    if (zbuffer[int(std::round(x)) + 625][int(std::round(y)) + 381] <= z || zbuffer[int(std::round(x)) + 625][int(std::round(y)) + 381] == 0) {
                        drawPoint(int(std::round(x)), int(std::round(y)), R, G, B);
                        zbuffer[int(std::round(x)) + 625][int(std::round(y)) + 381] = z;
                    }
                }
            }
        } else {
            //рисовать линию
            for (x = std::round(x1); x >= std::round(x2); x--) {
                //интенсивность в точке x
                z = z2 + (z1 - z2) * ((x - x2) / (x1 - x2));
                t = (x2 - x) / (x2 - x1);
                I = t * I1 + (1 - t) * I2;
                //поставить точку

                R = int(Cr * I);
                G = int(Cg * I);
                B = int(Cb * I);

                if (R > 255) {
                    R = 255;
                }

                if (R < 0) {
                    R = 0;
                }

                if (G > 255) {
                    G = 255;
                }

                if (G < 0) {
                    G = 0;
                }

                if (B > 255) {
                    B = 255;
                }

                if (B < 0) {
                    B = 0;
                }

                if (x >= -625 && y >= -381 && x <= 652 && y <= 381) {
                    if (zbuffer[int(std::round(x)) + 625][int(std::round(y)) + 381] <= z || zbuffer[int(std::round(x)) + 625][int(std::round(y)) + 381] == 0) {
                        drawPoint(int(std::round(x)), int(std::round(y)), R, G, B);
                        zbuffer[int(std::round(x)) + 625][int(std::round(y)) + 381] = z;
                    }
                }
            }
        }
    }
    scene->update();
}

void
ObjectDrawer::PaintSideGuro(double** zbuffer, Vector<Point> side, int n, light L, double kd, int Cr, int Cg, int Cb) {
    for (int i = 1; i < n - 1; i++) {
        AreaGuro(zbuffer, L, kd, side[0], side[i], side[i + 1], Cr, Cg, Cb);
    }
}

void ObjectDrawer::drawLine(Point start, Point end) {
    if ((start.getX() == end.getX()) && (start.getY() == end.getY())) {
        drawPoint(start.getX(), start.getY());
        return;
    }

    double dx = int(end.getX() - start.getX());
    double dy = int(end.getY() - start.getY());

    double sx = (dx < 0) ? -1 : 1;
    double sy = (dy < 0) ? -1 : 1;
    dx = abs(dx);
    dy = abs(dy);

    double x = start.getX();
    double y = start.getY();

    bool change = false;

    if (dy > dx) {
        double temp = dx;
        dx = dy;
        dy = temp;

        change = true;
    }

    double tg = abs(dy / dx);
    double error = tg - 0.5;

    int iterator = 1;
    while (iterator <= dx) {
        drawPoint(x, y);

        if (error >= 0) {
            if (change == false) y += sy;
            else x += sx;
            error--;
        }

        if (error < 0) {
            if (change == false) x += sx;
            else y += sy;
            error += tg;
        }

        iterator++;
    }

    scene->update();
}

ObjectDrawer::ObjectDrawer(QGraphicsScene* scene) {
    this->scene = scene;
}

void ObjectDrawer::drawFace(double** zbuffer, Vector<Point> vertices, Face face) {
    Vector<Point> side = {vertices[face.getA()], vertices[face.getB()], vertices[face.getC()]};

    light L;
    L.C = Point(200, -200, 200);
    L.I = 6;

    AreaGuro(zbuffer, L, 0.9, side[0], side[1], side[2], 0, 255, 0);
//    drawLine(vertices[face.getA()], vertices[face.getB()]);
//    drawLine(vertices[face.getB()], vertices[face.getC()]);
//    drawLine(vertices[face.getC()], vertices[face.getA()]);
}

void ObjectDrawer::drawFaces(double** zbuffer, Vector<Point> vertices, Vector<Face> faces) {
    Vector<Point> points(3);

    for (int i = 0; i < 1255; i++) {
        for (int j = 0; j < 762; j++) {
            zbuffer[i][j] = 0;
        }
    }

    for (int i = 0; i < faces.getSize(); i++) {
        points[0] = vertices[faces[i].getA()];
        points[1] = vertices[faces[i].getB()];
        points[2] = vertices[faces[i].getC()];

        drawFace(zbuffer, vertices, faces[i]);
    }
}

void ObjectDrawer::drawPoint(double x, double y, int r, int g, int b) {
    Drawer* drawer = new Drawer();

    drawer->setX(int(x));
    drawer->setY(int(y));
    drawer->setColor(r, g, b);

    scene->addItem(drawer);
}

void ObjectDrawer::drawPoint(double x, double y) {
    Drawer* drawer = new Drawer();

    drawer->setX(int(x));
    drawer->setY(int(y));

    scene->addItem(drawer);
}

void ObjectDrawer::sceneClear() {
    scene->clear();
}

void ObjectDrawer::setColor(QPen color) {
    this->color = color;
}

bool ObjectDrawer::isEqual(double x, double y) {
    return std::fabs(x - y) < std::numeric_limits<double>::epsilon();
}

