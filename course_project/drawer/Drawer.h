//
// Created by Marseille Gulaya on 26/11/2018.
//

#ifndef COURSEPROJECT_DRAWER_H
#define COURSEPROJECT_DRAWER_H

#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QPainter>
#include <cmath>

class Drawer: public QGraphicsItem
{
private:
    int x = 0;
    int y = 0;
    QPen color = QPen(Qt::darkGray);

public:
    QRectF boundingRect() const override;
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

    void setX(int x);
    void setY(int y);
    void setColor(int r, int g, int b);
};

#endif //COURSEPROJECT_DRAWER_H
